﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <sstream>
#include <ctime>

//void Print_num(int N, int k)
//{
    //for (int i = k; i <= N; i += 2)
       // std::cout << i << " ";
    //std::cout << "\n";
//}

//int** Create(size_t n, size_t m)
//{
    //int** M = new int* [n];
    //for (size_t i = 0; i < n; ++i)
    //{
        //M[i] = new int[m];
    //}
    //return M;
//}

//void Free(int** M, size_t n)
//{
    //for (size_t i = 0; i < n; ++i)
    //{
        //delete[] M[i];
    //}
    //delete[] M;
//}

//void Input(int** M, size_t n, size_t m)
//{
    //for (int i = 0; i < n; ++i)
    //{
        //for (int j = 0; j < m; ++j)
        //{
            //M[i][j] = i + j;
        //}
    //}
//}

//void Print(int** M, size_t n, size_t m)
//{
    //for (size_t i = 0; i < n; ++i)
    //{
        //for (size_t j = 0; j < m; ++j)
        //{
            //std::cout << M[i][j] << ' ';
        //}
        //std::cout << std::endl;
    //}
//}

int main()
{
    //int N;
    //std::cout << "N="; 
    //std::cin >> N;
    //std::cout << "Even:\n";
    //Print_num(N, 0);
    //std::cout << "Odd;\n";
    //Print_num(N, 1);

    //system("pause");
    //return 0;


    //int a = 8;
    //int b = 9;

    //std::cout << a + b << "\n";
    //std::cout << a - b << "\n";
    //std::cout << a * b << "\n";
    //std::cout << a / b << "\n";
    //std::cout << b % a << "\n";

    //a += b; // a - a + b

    //int x = 0;
    //std::cout << "x = " << x << "\n";
    //int y = x++; //x--
    //std::cout << "y = x++" << " => y = " << y << ,x = " << x << '\n';
    //y = ++x;
    //std::cout << "y = ++x" => y = " << y << ", x = " << x << '\n';

    //bool c = 0 < 1; // <, >, <=,  >=, ==, !=
    //bool d = false;
    //std::cout << "\n c = " << c << ", d = " << d;
    //std::cout << "\n c && d = " << (c && d); // И
    //std::cout << "\n c || d = " << (c || d); // Или
    //std::cout << "\n !d = " << !d; // Отрицание
    //std::cout << "\n !(c && d) = " << !(c && d) << '\n';

    //if (false)
    //{
    //     std::cout << "true\n";
    //}
    //else
    //{
    //    std::cout << "else\n";
    //}

    //int a = 0;
    //if (a!=0)
    //{
    //    std::cout << "true\n";
    //}
    //else if(a==0)
    //{
    //    std::cout << "else!\n";
    //}

    //int a = 0;
    //switch (a)
    //{
    //case 1: //a==1
      //  std::cout << "a == 1";
      //  break;
    //case 0: //a==0
      //  std::cout << "a == 0";
      //  break;
    //default:
      //  std::cout << "default";
      //  break;
    //}

    // Циклы

    //int i = 0;
    //while (i < 5)
    //{
      //  i++;
      //  std::cout << "yay\n";
    //}

    //for (int i = 0; i < 10; ++i)
    //{
     //   std::cout << "for iteration!\n";
    //}

    // Урок 16

    //const int size = 10;
    //int array[size] = { 1,2,3,4,5,6,7,8,9,10 };
    //std::cout << array[0]; Вызов одного числа из массива
    //for (int i = 0; i < size; i++)
    //{
      //  std::cout << array[i];         Вызов всего массива
    //}

    //const int size = 2;   // Двумерный массив
    //int array[size][size] = { {0, 1},{2, 3} };
        //for (int i = 0; i < size; i++)
    //{
        //for (int j = 0; j < size; j++)
        //{
            //std::cout << array[i][j];
        //}
        //std::cout << '\n';
    //}

//Преобразование типов данных
//Явное преобразование
//std::cout << static_cast<double>(5);
//auto a = 5.0;
//Приоритет операций в С++
//bool a = false;
//std::cout << (a || true);

// Домашнее задание 16

//size_t n;
//std::cout << "Input the size of matrix:";
//std::cin >> n;
//int** A = Create(n, n);
//Input(A, n, n);
//Print(A, n, n);

//Free(A, n);

//return 0;

//int day = 8;

//int sum = 0;

//int target_tow_index = day % N;

//for (int j = 0; j < N; j++)

//{

    //sum = sum + M[target_row_index][j];

//}

//std::cout << sum;

//std:: string str = "100";
//int num; 

//std::stringstream ss;
//ss << str;
//ss >> num;

//std::cout << "The string value is " << str << endl;
//std::cout << "The integer representation of the string is " << num << endl;

setlocale(LC_ALL, "rus");
int n, m, N, sum = 0;
std::cout << "Введите значение N: ";
std::cin >> N;
time_t t;
time(&t);
int k = (localtime(&t)->tm_mday) % N;
std::cout << "Введите размерность массива: ";
std::cin >> n >> m;
int** a = new int* [n];
for (int i = 0; i < n; i++)
    a[i] = new int[m];
std::cout << "Введите элементы массива:\n";
for (int i = 0; i < n; i++)
    for (int j = 0; j < m; j++) {
        std::cin >> a[i][j];
        if (i == k) sum += a[i][j];
    }
std::cout << "Сумма элементов строки " << k << " = " << sum;
for (int i = 0; i < n; i++)
    delete[]a[i];
delete[] a;
return 0;
}